# 使用教程

## debian or ubuntu
```shell
apt-get install curl -y
bash <(curl -s -L https://gitlab.com/x-x/ssr/-/raw/14bf3e7e50d5e06a70a8046cc83ed79dbce5e35f/ssrmu.sh)
```

## centos7

```shell
yum install curl -y
bash <(curl -s -L https://gitlab.com/x-x/ssr/-/raw/14bf3e7e50d5e06a70a8046cc83ed79dbce5e35f/ssrmu.sh)
```

## 系统优化脚本

```shell
bash <(curl -s -L https://git.io/optimize.sh)
```